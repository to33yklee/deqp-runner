use crate::parse_deqp::{parse_deqp_results_with_timeout, DeqpStatus};
use crate::runner_results::*;
use crate::{TestCase, TestCommand, TestConfiguration};
use anyhow::{Context, Result};
use log::*;
use regex::Regex;
use std::fs::File;
use std::io::prelude::*;
use std::io::{BufReader, BufWriter};
use std::path::{Path, PathBuf};
use std::process::{Command, Stdio};

pub struct DeqpCommand {
    pub deqp: PathBuf,
    pub args: Vec<String>,
    pub config: TestConfiguration,
    pub qpa_to_xml: Option<PathBuf>,
    pub prefix: String,
}

fn write_caselist_file(filename: &Path, tests: &[&TestCase]) -> Result<()> {
    let file = File::create(filename)
        .with_context(|| format!("creating temp caselist file {}", filename.display()))?;
    let mut file = BufWriter::new(file);

    for test in tests.iter() {
        file.write(test.name().as_bytes())
            .context("writing temp caselist")?;
        file.write(b"\n").context("writing temp caselist")?;
    }
    Ok(())
}

fn add_filename_arg(args: &mut Vec<String>, arg: &str, path: &Path) -> Result<()> {
    args.push(format!(
        "{}={}",
        arg,
        path.to_str()
            .with_context(|| format!("filename to utf8 for {}", path.display()))?
    ));
    Ok(())
}

impl DeqpCommand {
    fn try_extract_qpa<S: AsRef<str>, P: AsRef<Path>>(&self, test: S, qpa_path: P) -> Result<()> {
        let qpa_path = qpa_path.as_ref();
        let test = test.as_ref();
        let output = filter_qpa(
            File::open(qpa_path).with_context(|| format!("Opening {}", qpa_path.display()))?,
            test,
        )?;

        if !output.is_empty() {
            let out_path = qpa_path.parent().unwrap().join(format!("{}.qpa", test));
            // Write the extracted QPA contents to an individual file.
            {
                let mut out_qpa = BufWriter::new(File::create(&out_path).with_context(|| {
                    format!("Opening output QPA file {:?}", qpa_path.display())
                })?);
                out_qpa.write_all(output.as_bytes())?;
            }

            // Now that the QPA file is written (and flushed, note the separate
            // block!), call out to testlog-to-xml to convert it to an XML file
            // for display.
            if let Some(qpa_to_xml) = self.qpa_to_xml() {
                let xml_path = out_path.with_extension("xml");
                let convert_output = Command::new(qpa_to_xml)
                    .current_dir(self.deqp.parent().unwrap_or_else(|| Path::new("/")))
                    .arg(&out_path)
                    .arg(xml_path)
                    .output()
                    .with_context(|| format!("Failed to spawn {}", qpa_to_xml.display()))?;
                if !convert_output.status.success() {
                    anyhow::bail!(
                        "Failed to run {}: {}",
                        qpa_to_xml.display(),
                        String::from_utf8_lossy(&convert_output.stderr)
                    );
                } else {
                    std::fs::remove_file(&out_path).context("removing converted QPA")?;
                }
            }
        }

        Ok(())
    }
    pub fn qpa_text_check(&self, testcase: &str, regex: &str, log_name: &str) -> Result<bool> {
        let regex = Regex::new(regex)
            .with_context(|| format!("Compiling QPA text check RE '{}'", regex))?;
        let qpa_path = self
            .config
            .output_dir
            .canonicalize()
            .context("qpa check canonicalize")?
            .join(format!("{}.qpa", testcase));

        let mut args: Vec<String> = Vec::new();

        // Add on the user's specified deqp arguments.
        for arg in &self.args {
            args.push(arg.clone());
        }

        args.push(format!("--deqp-case={}", &testcase));
        add_filename_arg(&mut args, "--deqp-log-filename", &qpa_path)
            .context("adding log to args")?;

        let output = Command::new(&self.deqp)
            .current_dir(self.deqp.parent().unwrap_or_else(|| Path::new("/")))
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .stdin(Stdio::null())
            .args(args)
            // Disable MESA_DEBUG output by default for debug Mesa builds, which
            // otherwise fills the logs with warnings about GL errors that are
            // thrown (you're running deqp!  Of course it makes GL errors!)
            .env("MESA_DEBUG", "silent")
            .envs(self.config.env.iter())
            .output()
            .with_context(|| format!("Failed to spawn {}", &self.deqp.display()))?;

        if !output.status.success() {
            anyhow::bail!(
                "Failed to invoke dEQP for renderer check:\nstdout:\n{}\nstderr:\n{}",
                String::from_utf8_lossy(&output.stdout),
                String::from_utf8_lossy(&output.stderr)
            );
        }

        let mut qpa = String::new();
        File::open(qpa_path)
            .context("opening renderer check QPA")?
            .read_to_string(&mut qpa)
            .context("reading renderer check QPA")?;

        let renderer = if testcase == "dEQP-VK.info.device" {
            qpa.lines()
                .find(|x| x.contains("deviceName: "))
                .context("finding deviceName in QPA output")?
                .trim_start_matches("deviceName: ")
        } else {
            // XML regexing :(
            let text_re = Regex::new("<Text>(.*)</Text>").context("text match RE")?;
            let text_line = qpa
                .lines()
                .skip_while(|line| !line.contains("<TestCaseResult"))
                .nth(1)
                .context("reading line after TestCaseResult")?;

            let captures = text_re
                .captures(text_line)
                .context("Finding text line after TestCaseResult")?;
            captures.get(1).unwrap().as_str()
        };

        println!("{}: {}", log_name, &renderer);

        Ok(regex.is_match(renderer))
    }

    fn qpa_to_xml(&self) -> Option<&PathBuf> {
        self.qpa_to_xml.as_ref()
    }
}

impl TestCommand for DeqpCommand {
    fn run(
        &self,
        caselist_state: &CaselistState,
        tests_o: &[&TestCase],
    ) -> Result<Vec<RunnerResult>> {
        let caselist_path = self
            .caselist_file_path(caselist_state, "caselist.txt")
            .context("caselist path")?;
        let qpa_path = self
            .caselist_file_path(caselist_state, "qpa")
            .context("qpa path")?;
        let cache_path = self
            .config
            .output_dir
            .canonicalize()
            .context("cache path")?
            .join(format!("t{}.shader_cache", thread_id::get()));

        let mut tests: Vec<&TestCase> = Vec::new();
        for test in tests_o {
            tests.push(test);
        }

        write_caselist_file(&caselist_path, tests.as_slice()).context("writing caselist file")?;

        let mut args: Vec<String> = Vec::new();

        // Add on the user's specified deqp arguments.
        for arg in &self.args {
            args.push(arg.clone());
        }

        add_filename_arg(&mut args, "--deqp-caselist-file", &caselist_path)
            .context("adding caselist to args")?;
        add_filename_arg(&mut args, "--deqp-log-filename", &qpa_path)
            .context("adding log to args")?;
        args.push("--deqp-log-flush=disable".to_string());

        // The shader cache is not multiprocess safe, use one per
        // caselist_state.  However, since we're spawning lots of separate dEQP
        // runs, disable truncation (which would otherwise mean we only
        // get caching within a single run_block(), which is pretty
        // small).
        add_filename_arg(&mut args, "--deqp-shadercache-filename", &cache_path)
            .context("adding cache to args")?;
        args.push("--deqp-shadercache-truncate=disable".to_string());

        debug!(
            "Begin caselist c{}.r{}",
            caselist_state.caselist_id, caselist_state.run_id
        );

        let mut command = Command::new(&self.deqp);
        command
            .current_dir(self.deqp.parent().unwrap_or_else(|| Path::new("/")))
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .stdin(Stdio::null())
            .args(args)
            // Disable MESA_DEBUG output by default for debug Mesa builds, which
            // otherwise fills the logs with warnings about GL errors that are
            // thrown (you're running deqp!  Of course it makes GL errors!)
            .env("MESA_DEBUG", "silent")
            .envs(self.config.env.iter());

        let command_line = format!("{:?}", command);

        let mut child = command
            .spawn()
            .with_context(|| format!("Failed to spawn {}", &self.deqp.display()))?;

        let stdout = child.stdout.take().context("opening stdout")?;
        let caselist_results = parse_deqp_results_with_timeout(stdout, self.config.timeout);

        // The child should have run to completion based on parse_deqp_results() consuming its output,
        // but if we had a timeout or parse failure then we want to kill this run.
        let _ = child.kill();

        // Make sure we reap the child process.
        let child_status = child.wait().context("waiting for child")?;

        let caselist_results = caselist_results.context("parsing results")?;
        let mut deqp_results = caselist_results.results;

        for result in &mut deqp_results {
            result.name = format!("{}{}", self.prefix, result.name);
        }

        let stderr: Vec<String> = BufReader::new(child.stderr.as_mut().context("opening stderr")?)
            .lines()
            .flatten()
            .collect();

        for line in &stderr {
            // If the driver has ASan enabled and it detected leaks, then mark
            // all the tests in the caselist as failed (since we don't know who
            // to assign the failure to).
            if line.contains("ERROR: LeakSanitizer: detected memory leaks") {
                error!(
                    "deqp-runner: Leak detected, marking caselist as failed ({})",
                    self.see_more("", caselist_state)
                );
                for result in deqp_results.iter_mut() {
                    result.status = DeqpStatus::Fail;
                }
            }
            error!("dEQP error: {}", line);
        }

        let mut save_log = false;
        let mut results: Vec<RunnerResult> = Vec::new();
        for result in deqp_results {
            let status = self.translate_result(&result, caselist_state);

            if status.should_save_logs(self.config.save_xfail_logs) {
                save_log = true;
            }

            if !status.is_success() {
                if let Err(e) =
                    self.try_extract_qpa(result.name.trim_start_matches(&self.prefix), &qpa_path)
                {
                    warn!("Failed to extract QPA resuls: {}", e)
                }
            }

            results.push(RunnerResult {
                test: result.name,
                status,
                duration: result.duration,
                subtest: false,
            });
        }

        if save_log {
            let stdout = caselist_results.stdout;

            let log_path = self
                .caselist_file_path(caselist_state, "log")
                .context("log path")?;

            let mut file = File::create(log_path).context("opening log file")?;

            fn write_output(file: &mut File, name: &str, out: &[String]) -> Result<()> {
                if out.is_empty() {
                    writeln!(file, "{}: (empty)", name)?;
                } else {
                    writeln!(file, "{}:", name)?;
                    writeln!(file, "-------")?;
                    for line in out {
                        writeln!(file, "{}", line)?;
                    }
                }
                Ok(())
            }

            // Use a closure to wrap all the try operator paths with one .context().
            || -> Result<()> {
                writeln!(file, "command: {}", command_line)?;
                writeln!(file, "exit status: {}", child_status)?;
                write_output(&mut file, "stdout", &stdout)?;
                write_output(&mut file, "stderr", &stderr)?;
                Ok(())
            }()
            .context("writing log file")?;
        }

        // Something happens occasionally in runs (particularly with ASan) where
        // we get an -ENOENT from removing these files. We don't want to fail
        // the caselist for that if it has useful results.
        if !results.is_empty()
            && results
                .iter()
                .all(|x| !x.status.should_save_logs(self.config.save_xfail_logs))
        {
            if let Err(e) = std::fs::remove_file(&caselist_path)
                .with_context(|| format!("removing caselist at {:?}", &caselist_path))
            {
                error!("{:?}", e);
            }
        }
        if let Err(e) = std::fs::remove_file(&qpa_path)
            .with_context(|| format!("removing qpa at {:?}", &qpa_path))
        {
            error!("{:?}", e);
        };

        debug!(
            "End caselist c{}.r{}",
            caselist_state.caselist_id, caselist_state.run_id
        );

        Ok(results)
    }

    fn see_more(&self, _name: &str, caselist_state: &CaselistState) -> String {
        // This is the same as run() did, so we should be safe to unwrap.
        let qpa_path = self.config.output_dir.join(
            format!(
                "c{}.r{}.log",
                caselist_state.caselist_id, caselist_state.run_id
            )
            .as_str(),
        );
        format!("See {:?}", qpa_path)
    }

    fn config(&self) -> &TestConfiguration {
        &self.config
    }

    fn prefix(&self) -> &str {
        &self.prefix
    }
}

fn filter_qpa<R: Read, S: AsRef<str>>(reader: R, test: S) -> Result<String> {
    let lines = BufReader::new(reader).lines();

    let start = format!("#beginTestCaseResult {}", test.as_ref());

    let mut found_case = false;
    let mut including = true;
    let mut output = String::new();
    for line in lines {
        let line = line.context("reading QPA")?;
        if line == start {
            found_case = true;
            including = true;
        }

        if including {
            output.push_str(&line);
            output.push('\n');
        }

        if line == "#beginSession" {
            including = false;
        }

        if including && line == "#endTestCaseResult" {
            break;
        }
    }

    if !found_case {
        anyhow::bail!("Failed to find {} in QPA", test.as_ref());
    }

    Ok(output)
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn filter_qpa_success() {
        assert_eq!(
            include_str!("test_data/deqp-gles2-renderer.qpa"),
            filter_qpa(
                Cursor::new(include_str!("test_data/deqp-gles2-info.qpa")),
                "dEQP-GLES2.info.renderer"
            )
            .unwrap(),
        );
    }

    #[test]
    fn filter_qpa_no_results() {
        assert!(filter_qpa(
            Cursor::new(include_str!("test_data/deqp-empty.qpa")),
            "dEQP-GLES2.info.version"
        )
        .is_err());
    }
}
